from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Message(models.Model):
    title = models.CharField(max_length=50, null=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    body = models.TextField(null=False)
    id = models.AutoField(primary_key=True, null=False, unique=True)
    views = models.IntegerField(default=0, null=False)
    public = models.BooleanField(default=True, null=False)

    def __str__(self):
        return self.title[:50]

