var hamburgerContents = document.getElementById("hamburger-contents");
var fillNav = document.getElementById("fill-nav");

function showHamburger() {
    
    if (hamburgerContents.style.display === "flex"){
        setTimeout(() => {hamburgerContents.style.display = "none"}, 300)
        fillNav.style.display = "none"
        hamburgerContents.style.opacity = "0"
    } else{
        fillNav.style.display = "block"
        hamburgerContents.style.display = "flex"
        setTimeout(() => {hamburgerContents.style.opacity = "1"}, 0)
        
        
    }
    
}
function closeHamburger(){
    
    if (hamburgerContents.style.display === "flex"){
        setTimeout(() => {hamburgerContents.style.display = "none"}, 300)
        
        hamburgerContents.style.opacity = "0"
        fillNav.style.display = "none"
        
        
    }
}