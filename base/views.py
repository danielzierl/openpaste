from django.shortcuts import render,redirect, reverse
from django.http import HttpResponse,HttpResponseRedirect
# Create your views here.
from base.models import Message
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from slugify import slugify
from django.db.models import Q
import git

def get_page(request):
    if not 'page' in request.GET:
        return 1
    else:
        return int(request.GET['page'])

def get_page_max(messages_count):
    page_max = messages_count // 10
   
    page_max+= 1 if not messages_count % 10 == 0 else 0
    if(page_max==0):
        page_max+=1
    return page_max

def get_mode(request):
    if 'mode' in request.session:
        return request.session['mode']
    else:
        return 'light'

def index(request):
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    recent_messages = Message.objects.filter(public=True).order_by('-created_at')[:10]
    message = Message()
    message.public =True
    
    context = {
        "recent_messages":recent_messages,
        "sha":sha,
        "message":message,
        "mode":get_mode(request),
        
    }
    return render(request, 'base/index.html',context)

def profile(request,id):
    user = User.objects.get(id=id)

    page = get_page(request)
    all_messages_count = Message.objects.filter(owner=user).count()
    
    page_max = get_page_max(all_messages_count)
    
    if request.user == user:
        messages = Message.objects.filter(owner=user).order_by('-created_at')[(page-1)*10:(page)*10]
    else:
        messages = Message.objects.filter(owner=user, public=True).order_by('-created_at')[(page-1)*10:(page)*10]
    
    recent_messages = Message.objects.filter(public=True).order_by('-created_at')[:10]
    show_page_selector = not((page-1 == 0) and (page_max == page)) 
    
    context = {
        "messages":messages,
        "recent_messages":recent_messages,
        "user":user,
        "page_prev":page-1,
        "page_next":page+1,
        "page_max":page_max,
        "page":page,
        "show_page_selector":show_page_selector,
        "mode":get_mode(request),
        
    }
    return render(request, 'base/profile.html',context)
    
def login_view(request):
    
    if 'error_msg' in request.session:
        error_msg = request.session['error_msg']
        request.session['error_msg'] = None
    else:
       error_msg = None 
    context = {
        'error_msg':error_msg,
        "mode":get_mode(request),
    }
    return render(request, 'base/login.html',context)
    

def register(request):
    if request.method == 'POST':
        username = request.POST['username']
        request.session['username']=username
        password = request.POST['password']
        if User.objects.filter(username=username).exists():
            request.session['error_msg']='The user already exists'
            return redirect('base:register')
        if len(password)<7:
            request.session['error_msg']='Please use a password 8 characters or longer'
            return redirect('base:register')
        
        user = User.objects.create_user(username=username,password=password)
        user.save()
        login(request, user)
        return redirect('base:profile',user.id)
    
    context = {
        "mode":get_mode(request),
    }
    if 'username' in request.session:
        username = request.session['username']
        request.session['username'] = None
        context['username'] = username
    else:
        context['username'] = ""

        
    if 'error_msg' in request.session:
        error_msg = request.session['error_msg']
        request.session['error_msg'] = None
        context['error_msg'] = error_msg
    else:
       context['error_msg'] = None 
    
    return render(request, 'base/register.html',context)

def authenticate_view(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        request.session['error_msg']=None
        login(request, user)
        return redirect('base:profile', user.id)
    else:
        request.session['error_msg']='Username or password is incorrect'
        return redirect('base:login')
    
def detail(request, id):
    message:Message = Message.objects.get(id=id)

    if request.method == 'POST':
        message.body = request.POST['content']
        message.title = request.POST['title']
        message.public = bool(int(request.POST['public']))
        message.save()
    if not message.public and not message.owner == request.user:
        return redirect('base:index')


    context ={
        'message':message,
        "mode":get_mode(request),
    }
    message.views +=1
    message.save()
    return render(request, 'base/detail.html',context)

def create(request):
    message = Message()
    if request.method == 'POST':
        message = Message.objects.create(title=request.POST['title'],
         body=request.POST['content'])
        if request.user.is_authenticated:
            message.owner = request.user
            message.public = bool(int(request.POST['public']))
            message.save()
        request.session['back_url'] = request.META['HTTP_REFERER']
        return redirect('base:detail', message.id)
    context = {
        "mode":get_mode(request),
        'message':message,
        "show_back_button":True,
    }
    return render(request, 'base/create.html',context)

def edit(request,id=1):
    message:Message = Message.objects.get(id=id)
    if message.owner == request.user:
        context ={
            'message':message,
            "show_back_button":True,
            "mode":get_mode(request),
        }
        return render(request, 'base/edit.html',context)
    
def raw(request,id):
    message:Message = Message.objects.get(id=id)
    context ={
        'message':message
    }
    return render(request, 'base/raw.html',context)

def logout_view(request):
    logout(request)
    return redirect('base:index')

def download_view(request, id):
    message:Message = Message.objects.get(id=id)
    file_data = message.body
    filename = slugify(message.title[:50])
    if len(filename)==0:
        filename = 'dowloaded-file'
    response = HttpResponse(file_data, content_type='application/text charset=utf-8')
    response['Content-Disposition'] = f'attachment; filename="{filename}.txt"'
    return response

def search(request):
    recent_messages = Message.objects.filter(public=True).order_by('-created_at')[:10]
    q = request.GET['q']
    
    page = get_page(request)
    messages_raw = Message.objects.filter((Q(owner__username__icontains=q) | Q(title__icontains=q)) & Q(public=True))
    messages_count = messages_raw.count()
    messages = messages_raw[(page-1)*10:(page)*10]



    page_max = get_page_max(messages_count)
    

    show_page_selector = not((page-1 == 0) and (page_max == page)) 
    context = {
        "recent_messages":recent_messages,
        "messages":messages,
        "page_prev":page-1,
        "page_next":page+1,
        "page_max":page_max,
        "page":page,
        "show_page_selector":show_page_selector,
        "q":q,
        "mode":get_mode(request),
    }
    return render(request, 'base/search.html', context)

def delete(request, id):
    message = Message.objects.get(id=id)
    if not request.user == message.owner:
        return redirect('base:detail', message.id)

    if request.method == 'POST':
        deletion_bool:bool= True if request.POST['delete'] =="Yes" else False;
        if deletion_bool:
            message.delete()
            return redirect('base:profile', request.user.id)
        else:
            return redirect('base:detail', message.id)



    context = {
        'message':message,
        "mode":get_mode(request),
    }
    return render(request, 'base/delete.html',context)

def change_mode(request):
    if not 'mode' in request.session or request.session['mode'] == 'light':
        request.session['mode'] = 'dark'
    else:
        request.session['mode'] = 'light'
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def back(request):
    if 'back_url' in request.session:
        return HttpResponseRedirect(request.session.pop('back_url'))
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
