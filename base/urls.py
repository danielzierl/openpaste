from django.contrib import admin
from django.urls import path,include
import base.views as views

app_name='base'

urlpatterns = [
    path('', views.index, name='index'),
    path('profile/<int:id>', views.profile, name='profile'),
    path('login/', views.login_view, name='login'),
    path('register/', views.register, name='register'),
    path('authenticate/', views.authenticate_view, name='authenticate'),
    path('detail/<int:id>/', views.detail, name='detail'),
    path('create/', views.create, name='create'),
    path('edit/<int:id>/', views.edit, name='edit'),
    path('raw/<int:id>/', views.raw, name='raw'),
    path('logout/', views.logout_view, name='logout'),
    path('download/<int:id>', views.download_view, name='download'),
    path('search/',views.search, name='search'),
    path('delete/<int:id>',views.delete, name='delete'),
    path('change-mode/',views.change_mode, name='change_mode'),
    path('back/',views.back, name='back'),
]